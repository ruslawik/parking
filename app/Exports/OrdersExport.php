<?php

namespace App\Exports;

use Carbon\Carbon;
use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdersExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function collection()
    {
        //returns Data with User data, all user data, not restricted to start/end dates
        return Order::where('date_time_from', '>=', $this->from)
                            ->where('date_time_from', '<=', $this->to)
                            ->get();
    }

    public function map($order) : array {
        return [
            $order->id,
            $order->date_time_from,
            $order->date_time_to,
            $order->name,
            $order->phone,
            $order->car_num,
            $order->email,
            $order->status,
            $order->parking_and_floor($order->place_id)
        ];
    }

    public function headings():array
    {
        return ["Номер брони", "Дата и время 'С'", "Дата и время 'ДО'", "ФИО", "Номер телефона", "Номер авто", "Электронная почта", "Статус оплаты", "Парковка, этаж и место"];
    }
}
