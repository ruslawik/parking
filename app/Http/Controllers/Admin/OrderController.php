<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyOrderRequest;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Order;
use App\Place;
use App\Parking;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Exports\OrdersExport;
use Maatwebsite\Excel\Facades\Excel;

class OrderController extends Controller 
{
    public function index()
    {
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $orders = Order::all();
        //return Excel::download(new OrdersExport, 'orders.xlsx');
        return view('admin.orders.index', compact('orders'));
    }

    public function download_report(Request $r){

        return Excel::download(new OrdersExport($r->input('date_time_from'), $r->input('date_time_to')), 'отчет.xlsx');
    }

    public function create()
    {
        abort_if(Gate::denies('order_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $places = Place::all()->pluck('place', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.orders.create', compact('places'));
    }

    public function store(StoreOrderRequest $request)
    {
        $order = Order::create($request->all());

        return redirect()->route('admin.orders.index');
    }

    public function edit(Order $order)
    {
        abort_if(Gate::denies('order_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $order->load('place');
        $places = Place::where('parking_id', $order->place->parking_id)
                        ->where('floor', $order->place->floor)->pluck('place', 'id')->prepend(trans('global.pleaseSelect'), '');
        $parkings = Parking::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $floors = Place::where('parking_id', $order->place->parking_id)
                        ->groupBy('floor')
                        ->get();

        return view('admin.orders.edit', compact('places', 'order', 'parkings', 'floors'));
    }

    public function get_floors(Request $r){

        $ar['floors'] = Place::where('parking_id', $r->input('parking_id'))
                        ->groupBy('floor')
                        ->get();

        return view('admin.orders.floors_for_edit_order', $ar);
    }

    public function get_places(Request $r){

        $ar['places'] = Place::where('parking_id', $r->input('parking_id'))
                        ->where('floor', $r->input('floor'))
                        ->get();
        $ar['date_from'] = $r->input('date_time_from');
        $ar['date_to'] = $r->input('date_time_to');

        return view('admin.orders.places_for_edit_order', $ar);
    }

    public function update(UpdateOrderRequest $request, Order $order)
    {
        $order->update($request->all());

        return redirect()->route('admin.orders.index');
    }

    public function show(Order $order)
    {
        abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $order->load('place');

        return view('admin.orders.show', compact('order'));
    }

    public function destroy(Order $order)
    {
        abort_if(Gate::denies('order_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $order->delete();

        return back();
    }

    public function massDestroy(MassDestroyOrderRequest $request)
    {
        Order::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
