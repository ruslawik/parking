<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyParkingRequest;
use App\Http\Requests\StoreParkingRequest;
use App\Http\Requests\UpdateParkingRequest;
use App\Parking;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ParkingController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('parking_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parkings = Parking::all();

        return view('admin.parkings.index', compact('parkings'));
    }

    public function create()
    {
        abort_if(Gate::denies('parking_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.parkings.create');
    }

    public function store(StoreParkingRequest $request)
    {
        $parking = Parking::create($request->all());

        return redirect()->route('admin.parkings.index');
    }

    public function edit(Parking $parking)
    {
        abort_if(Gate::denies('parking_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.parkings.edit', compact('parking'));
    }

    public function update(UpdateParkingRequest $request, Parking $parking)
    {
        $parking->update($request->all());

        return redirect()->route('admin.parkings.index');
    }

    public function show(Parking $parking)
    {
        abort_if(Gate::denies('parking_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parking->load('parkingPlaces');

        return view('admin.parkings.show', compact('parking'));
    }

    public function destroy(Parking $parking)
    {
        abort_if(Gate::denies('parking_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parking->delete();

        return back();
    }

    public function massDestroy(MassDestroyParkingRequest $request)
    {
        Parking::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
