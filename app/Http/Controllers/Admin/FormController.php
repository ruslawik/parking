<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Parking;
use App\Place;
use Carbon\Carbon;
use App\Order;
use DateTime;
use DateTimeZone;

class FormController
{

    public function date_time_to_ajax(Request $r){
        $order_id = $r->input('order_id');
        $data = Order::where('id', $order_id)->get();
        if($data[0]->check_time_to == 0){
            Order::where('id', $order_id)->update(["check_time_to" => 1]);
        }else{
            Order::where('id', $order_id)->update(["check_time_to" => 0]);
        }
    }  

    public function date_time_from_ajax(Request $r){
        $order_id = $r->input('order_id');
        $data = Order::where('id', $order_id)->get();
        if($data[0]->check_time_from == 0){
            Order::where('id', $order_id)->update(["check_time_from" => 1]);
        }else{
            Order::where('id', $order_id)->update(["check_time_from" => 0]);
        }
    }

    public function index()
    {	
    	$ar['all_parkings'] = Parking::all();
        return view('form', $ar);
    }

    public function order_bitein_dep_jatyr(){

        $orders = Order::where('is_finished', 'in_process')->get();

        foreach ($orders as $order) {
            $datetimeObj2 = Carbon::createFromFormat('Y-m-d H:i:s', $order->date_time_to);
            $datetimeObj1 = Carbon::now()->addHours(6);
            $minutes = $datetimeObj1->diffInMinutes($datetimeObj2, false);

            if($minutes >= 1 && $minutes <= 179){

                    $place_id = $order->place_id;
                    $place_data = Place::where('id', $place_id)->get();
                    $park_data = Parking::where('id', $place_data[0]->parking_id)->get();
                    
                    $to  = "<".$order->email.">";

                    $subject = "Истекает период аренды парковочного места (в течение 3-ех часов)"; 

                    $message = '<p>Здравствуйте, '.$order->name.'! В течение 3-ех часов истекает период аренды парковочного места. </p><p>Детали бронирования:</p> Номер брони: <b>'.$order->id.'</b></br>Парковка: <b>'.$park_data[0]->name.'</b> </br> Время заезда: <b>'.$order->date_time_from.'</b><br>Время выезда: <b>'.$order->date_time_to.'</b></br> Этаж/Место: <b>'.$place_data[0]->floor.'/'.$place_data[0]->place.'</b><br>Статус оплаты: <b>'.$order->status.'</b>';


                    $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
                    $headers .= "From: ZHKN Parking <parking@zhkn.kz>\r\n"; 
                    $headers .= "Reply-To: parking@zhkn.kz\r\n";

        
                    $url = "https://smsc.ru/sys/send.php?translit=1&login=ruslawik&psw=Amir159!&phones=".$order->phone."&nl=1&mes=В течение 3-ех часов истекает период аренды парковочного места%0AНомер брони: ".$order->id."%0A".$park_data[0]->name."%0A"."С: ".$order->date_time_from."%0A"."По: ".$order->date_time_to."%0AЭтаж/Место: ".$place_data[0]->floor.'/'.$place_data[0]->place."%0A".$order->status;

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL,$url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                    $returned = curl_exec($ch);
                    curl_close ($ch);

                    mail($to, $subject, $message, $headers);

                    Order::where('id', $order->id)->update(["is_finished"=>"finished"]);
            }

        }

    }

    public function booking (Request $r){

        $parking_id = $r->input('parking_id');
        $park_data = Parking::where('id', $parking_id)->get();
        $place_id = $r->input('place');
        $date_time_from =  Carbon::createFromFormat('Y-m-d H:i', $r->input('date_time_in'));
        $date_time_to = Carbon::createFromFormat('Y-m-d H:i', $r->input('date_time_out'));
        $name = $r->input('fio');
        $car_num = $r->input('nomer_auto');
        $phone = $r->input('phone_number');
        $email = $r->input('email');
        $payment_status = 'Оплачено '.$r->input('price');

        $phone = preg_replace("/\(/", "", $phone);
        $phone = preg_replace("/\)/", "", $phone);
        $phone = preg_replace("/\-/", "", $phone);
        $phone = preg_replace("/\s/", "", $phone);

        $ar['phone'] = $phone;
        $ar['email'] = $email;

        $order = new Order();
        $order->place_id = $place_id;
        $order->date_time_from = $date_time_from;
        $order->date_time_to = $date_time_to;
        $order->name = $name;
        $order->car_num = strtoupper($car_num);
        $order->phone = $phone;
        $order->email = $email;
        $order->status = $payment_status;
        $order->save();

        $place_data = Place::where('id', $place_id)->get();

        
        $to  = "<".$email.">";

        $subject = "Забронировано парковочное место"; 

        $message = '<p>Здравствуйте, '.$name.'! Вы успешно забронировали парковочное место.</p> <p>Детали бронирования:</p> <p> Номер брони: <b>'.$order->id.'</b><br> Парковка: <b>'.$park_data[0]->name.'</b><br> Время заезда: <b>'.$date_time_from.'</b><br> Время выезда: <b>'.$date_time_to.'</b><br> Этаж/Место: <b>'.$place_data[0]->floor.'/'.$place_data[0]->place.'</b><br> Статус оплаты: <b>'.$payment_status.'</b>';

        $message2 = 'Имя: <b>'.$name.'</b><br><br>Детали бронирования: <br><br>Номер брони: <b>'.$order->id.'</b><br>Парковка: <b>'.$park_data[0]->name.'</b> <br>Время заезда: <b>'.$date_time_from.'</b><br>Время выезда: <b>'.$date_time_to.'</b><br>Этаж/Место: <b>'.$place_data[0]->floor.'/'.$place_data[0]->place.'</b><br>Статус оплаты: <b>'.$payment_status.'</b>';

        $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
        $headers .= "From: ZHKN Parking <parking@zhkn.kz>\r\n"; 
        $headers .= "Reply-To: parking@zhkn.kz\r\n";

        
        $url = "https://smsc.ru/sys/send.php?translit=1&login=ruslawik&psw=Amir159!&phones=".$phone."&nl=1&mes=Забронировано парковочное место%0AНомер брони: ".$order->id."%0A".$park_data[0]->name."%0A"."С: ".$date_time_from."%0A"."По: ".$date_time_to."%0AЭтаж/Место: ".$place_data[0]->floor.'/'.$place_data[0]->place."%0A".$payment_status;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $returned = curl_exec($ch);
        curl_close ($ch);

        mail($to, $subject, $message, $headers);
        mail("inetbuilding.adwords@gmail.com", $subject, $message2, $headers);


        return $park_data[0]->name;
    }

    public function getPlacesAndPrice (Request $r){

    	$parking_id = $r->input('parking_id');
    	$ar['park_data'] = Parking::where('id', $parking_id)->get();
    	$ar['places'] = Place::where('parking_id', $parking_id)->groupBy('floor')->get();

    	return view('places_and_price', $ar);
    }

    public function getPlacesSelect (Request $r){

    	$ar['places'] = Place::where('parking_id', $r->input('parking_id'))
    							->where('floor', $r->input('floor'))
    							->get();
        $ar['date_from'] = $r->input('date_time_in');
        $ar['date_to'] = $r->input('date_time_out');

    	return view('places_select', $ar);
    }

    public function getPrice(Request $r){

    	$ar['price'] = Place::where('id', $r->input('place'))
    							->get();

        if($ar['price']->count() < 1){
            $ar['total'] = "Нет мест";
            $ar['formula'] = "Выберите другой этаж или дату";
            $ar['price_integer'] = 0;
            return $ar;
        }

    	$from_string = $r->input("date_time_in");
    	$to_string = $r->input("date_time_out");

    	$price = $ar['price'][0]->price;

    	$from = Carbon::createFromFormat('Y-m-d H:i', $from_string, 'Asia/Almaty');
    	$to = Carbon::createFromFormat('Y-m-d H:i', $to_string, 'Asia/Almaty');

    	$diff = $to->diff($from)->days;
    	
        /*if($diff == 0){
    		$diff = 1;
    	}*/
        $diff += 1;

    	if($diff > 30 && $diff <= 180) $price = $price - env('SKIDKA');
    	if($diff > 180) $price = $price - (env('SKIDKA')*2);

    	$ar['total'] = $diff*$price." тенге";
    	$ar['formula'] = "кол. суток: ".$diff." * "."цена за сутки: ".$price." = ".($diff*$price)." тенге";
        $ar['price_integer'] = $diff*$price;

    	return $ar;
    }
}
