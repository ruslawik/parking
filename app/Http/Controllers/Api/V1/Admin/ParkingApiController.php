<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreParkingRequest;
use App\Http\Requests\UpdateParkingRequest;
use App\Http\Resources\Admin\ParkingResource;
use App\Parking;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ParkingApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('parking_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ParkingResource(Parking::all());
    }

    public function store(StoreParkingRequest $request)
    {
        $parking = Parking::create($request->all());

        return (new ParkingResource($parking))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Parking $parking)
    {
        abort_if(Gate::denies('parking_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ParkingResource($parking);
    }

    public function update(UpdateParkingRequest $request, Parking $parking)
    {
        $parking->update($request->all());

        return (new ParkingResource($parking))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Parking $parking)
    {
        abort_if(Gate::denies('parking_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parking->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
