<?php

namespace App\Http\Requests;

use App\Place;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePlaceRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('place_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'floor'      => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'row'        => [
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'place'      => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'price'      => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'parking_id' => [
                'required',
                'integer',
            ],
        ];
    }
}
