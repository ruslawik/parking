<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;
use App\Order;

class Place extends Model
{
    use SoftDeletes;

    public $table = 'places';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const STATUS_SELECT = [
        'on'  => 'В работе',
        'off' => 'Не работает',
    ];

    protected $fillable = [
        'floor',
        'place',
        'price',
        'parking_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function placeOrders()
    {
        return $this->hasMany(Order::class, 'place_id', 'id');
    }

    public function parking()
    {
        return $this->belongsTo(Parking::class, 'parking_id');
    }

    public function is_free($place_id, $date_from, $date_to){

        $orders_check_1 = Order::where('place_id', $place_id)
                            ->where('date_time_from', '>=', $date_from)
                            ->where('date_time_from', '<=', $date_to)
                            ->get();

        $orders_check_2 = Order::where('place_id', $place_id)
                            ->where('date_time_to', '>=', $date_from)
                            ->where('date_time_to', '<=', $date_to)
                            ->get();

        if($orders_check_2->count() > 0 || $orders_check_1->count() > 0){
            return false;
        }else{
            return true;
        }

    }
}
