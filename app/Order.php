<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;
use App\Parking;
use App\Place;

class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';

    const STATUS_SELECT = [
        'bad'  => 'Не оплачено',
        'good' => 'Оплачено',
    ];

    protected $dates = [
        'date_time_from',
        'date_time_to',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'date_time_from',
        'date_time_to',
        'name',
        'phone',
        'car_num',
        'email',
        'status',
        'place_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i');
    }

    public function getDateTimeFromAttribute($value)
    {
        return $value;
    }

    public function setDateTimeFromAttribute($value)
    {
        $this->attributes['date_time_from'] = $value;
    }

    public function getDateTimeToAttribute($value)
    {
        return $value;
    }

    public function setDateTimeToAttribute($value)
    {
        $this->attributes['date_time_to'] = $value;
    }

    public function place()
    {
        return $this->belongsTo(Place::class, 'place_id');
    }

    public function parking_and_floor($place_id) {

        $place = Place::where("id", $place_id)->get();
        $park = Parking::where("id", $place[0]->parking_id)->get();

        return $park[0]->name."/ Этаж: ".$place[0]->floor."/ Место: ".$place[0]->place;

    }
}
