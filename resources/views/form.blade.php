<!doctype html>
<html lang="en">
<head>
    <title>Онлайн-бронирование парковки</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Include Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css" rel="stylesheet" type="text/css" />

    <script src="https://widget.cloudpayments.ru/bundles/cloudpayments"></script>

    <!-- Include SmartWizard CSS -->
    <link href="/dist/css/smart_wizard.css" rel="stylesheet" type="text/css" />

    <!-- Optional SmartWizard theme -->
    <link href="/dist/css/smart_wizard_theme_circles.css" rel="stylesheet" type="text/css" />
    <link href="/dist/css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />
    <link href="/dist/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />
    <link href="/dist/css/jquery.datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/dist/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body style="overflow-x: hidden !important;">
    <div id="thanks" style="display: none;">
        <center>Ваше место успешно забронировано! <br>Все данные высланы по указанным контактам.<br><a href="/form">Забронировать еще</a></center>
    </div>
    <!-- SmartWizard html -->
    <div id="smartwizard">
        <ul>
            <li>
                <a href="#step-1" class="tab_button active">
                    <span class="txt">
                        Шаг 1<br /><small>Бронирование</small>
                    </span>
                    <picture>
                        <img src="/dist/img/parking-sign%202.png" alt="">
                        <img src="/dist/img/parking-sign.png" alt="" class="colourly">
                    </picture>
                </a>
            </li>
            <li>
                <a href="#step-2" class="tab_button">
                    <span class="txt">
                        Шаг 2<br /><small>Оплата</small>
                    </span>
                    <picture>
                        <img src="/dist/img/credit-card%202.png" alt="">
                        <img src="/dist/img/credit-card.png" alt="" class="colourly">
                    </picture>
                </a>
            </li>
        </ul>

        <div style="margin-top: 20px;">
            <div id="step-1" class="">
                <h4 style="margin-left:20px !important;">Онлайн бронирование парковки</h4>
                <div class="row" style="padding:20px;">
                    <div class="form-group col-sm-4">
                        <label for="parking_select">Парковка:</label>
                        <div class="dropdown">
                            <select id="parking_select_id" onchange="date_time_changed();" class="form-control" name="parking_select">
                                @foreach($all_parkings as $parking)
                                    <option value="{{$parking->id}}">{{$parking->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-sm-4 calendar_input">
                        <label for="date_time_in">Дата и время заезда:</label>
                        
                        <input id="date_time_in" class="form-control" type="text" onchange="date_time_changed();" name="date_time_in">
                        <img src="/dist/img/timetable.png" class="calendar_icon">
                    </div>
                    <div class="form-group col-sm-4 calendar_input">
                        <label for="date_time_out">Дата и время выезда:</label>
                        
                        <input id="date_time_out" class="form-control" type="text" onchange="date_time_changed();" name="date_time_out">
                        <img src="/dist/img/timetable.png" class="calendar_icon">
                    </div>
                </div>
                <div id="loading" style="display: none;">
                    <div class="cssload-squares">
                        <span></span><span></span><span></span><span></span><span></span>
                    </div>
                </div>
                <div class="alert alert-danger alert-error" style="display:none">* минимальный срок бронирования - 1 час</div>
                <div id="place_choosing_div" style="display: none;">
                </div>
            </div>
            <div id="step-2" class="">
                <h4 style="margin-left:24px !important;">Введите данные и оплатите бронь</h4>
                <div class="row" style="padding:20px;margin-top:-30px;">
                    <div class="user-input-wrp">
                        <br>
                        <input type="text" id="fio" class="inputText" required/>
                        <span class="floating-label">ФИО водителя:</span>
                    </div>
                    <div class="user-input-wrp">
                        <br>
                        <input type="text" id="nomer_auto" class="inputText" required/>
                        <span class="floating-label">Номер авто: 123ABC01</span>
                    </div>
                    <div class="user-input-wrp">
                        <br>
                        <input id="phone_number" type="text" class="inputText" required/>
                        <span class="floating-label">Телефон:</span>
                    </div>
                    <div class="user-input-wrp">
                        <br>
                        <input type="text" id="email" class="inputText" required/>
                        <span class="floating-label">Эл. почта:</span>
                    </div>
                    <input type="hidden" id="price_hidden" value="0">
                    <input type="hidden" id="price_integer" value="0">
                </div>
                <br>
                <h3 style="margin-left:24px;">Вы выбрали:</h3>
                <div class="row" style="padding:20px;margin-top:-23px;" class="final_info">
                    <p style="margin-left:20px;">
                        Парковка: <b><span id="your_parking"></span></b><br>
                        Этаж: <b><span id="your_floor"></span></b> <br>
                        Место: <b><span id="your_place"></span></b><br>
                        Период парковки: <b>с <span id="your_date_from"></span> до <span id="your_date_to"></span></b><br><br>
                        <span class="accepting">
                            <input type="radio" name="shart" id="shart">
                            <label for="shart">Я принимаю <a style="color:black !important; text-decoration: underline;" href="https://zhkn.kz/dogovor-oferta/">Условия договора-оферты</a> и <a style="color:black !important; text-decoration: underline;" href="https://zhkn.kz/politika-konfidencialnosti/">Политику конфиденциальности</a></label>
                        </span>
                        <br><br>
                        <b>Стоимость:</b> <br> <span class="price" id="price_span_2"></span><br><br>
                         <button class="btn btn-secondary sw-btn-prev" style="float:left;margin-right: 10px;">Назад</button>
                         <button class="btn btn-secondary sw-btn-pay" id="checkout">Оплатить</button>
                    </p>
                </div>
            </div>
        </div>
    </div>

<h3 style="margin-left:24px;">Как забронировать парковочное место?</h3>
<p style="margin-left:24px;font-size:14px !important;">
1.  Выберите парковку, дату и время заезда, а также дату и время выезда<br>
2.  Выберите этаж и место<br>
3.  Заполните следующие поля: ФИО, номер авто, телефон и эл. почта<br>
4.  Поставьте галочку в пункте публичной оферты и политики конфиденциальности (ознакомиться с документами можно по соответствующим ссылкам)<br>
5.  Оплатите бронь с помощью банковской карты<br>
6.  После оплаты Вы получите смс-сообщение и эл. письмо с деталями брони<br>

</p>
    <br><br>

<!-- Include jQuery -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->

<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>

<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<!-- Include SmartWizard JavaScript source -->
<script type="text/javascript" src="/dist/js/jquery.smartWizard.js"></script>
<script type="text/javascript" src="/dist/js/jquery.maskedinput.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="/dist/js/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript">

    function date_time_changed (){


        let time_in = $('#date_time_in').val();
        let time_out = $('#date_time_out').val();
        console.log(time_out);
        if(time_out != '' && time_in != ''){
            time_in += ":00";
            time_out += ":00";
            let a = new Date(time_in.replace(/-/g, "/"));
            a.setHours(a.getHours() + 1);
            let b =  new Date(time_out.replace(/-/g, "/"));

            a_seconds = Date.parse(a);
            b_seconds = Date.parse(b);
            if(a_seconds <= b_seconds){
                $('.alert.alert-error').hide();
                show_free_places_and_price();
            }else{
                $('.alert.alert-error').show();
                $('#place_choosing_div').hide();
            }
            
        }
    }

    function load_places(){
         $.ajax({
                    url: "/get-places-select",
                    type: "post",
                    data: {
                        "_token": "{{csrf_token()}}",
                        "parking_id": $("#parking_select_id").val(),
                        "floor": $("#floor_select_id").val(),
                        "date_time_in": $('#date_time_in').val(),
                        "date_time_out": $('#date_time_out').val()
                    },
                    success: function (response) {
                        $("#places_select_div").html(response);
                        load_price();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
    }

    function show_free_places_and_price(){
          $.ajax({
                    url: "/get-free-places-and-price",
                    type: "post",
                    data: {
                        "_token": "{{csrf_token()}}",
                        "parking_id": $("#parking_select_id").val(),
                        "date_time_in": $('#date_time_in').val(),
                        "date_time_out": $('#date_time_out').val()
                    },
                    beforeSend: function() {
                        $("#loading").show();
                    },
                    success: function (response) {
                        $('#place_choosing_div').empty();
                        $('#place_choosing_div').html(response);
                        load_places();
                        $("#loading").hide();
                        $('#place_choosing_div').show();
                        $(".sw-btn-next").on("click", function() {
                            $('#smartwizard').smartWizard("next");
                            return true;
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
    }

    function load_price(){

        $("#price_span").html('Считаем...');
                        $.ajax({
                            url: "/get-price",
                            type: "post",
                            data: {
                                "_token": "{{csrf_token()}}",
                                "parking_id": $("#parking_select_id").val(),
                                "floor": $("#floor_select_id").val(),
                                "place": $("#places_select").val(),
                                "date_time_in": $('#date_time_in').val(),
                                "date_time_out": $('#date_time_out').val(),
                            },
                            success: function (response) {
                                if(response['total'] == "Нет мест"){
                                    $(".sw-btn-next").css('display', 'none');
                                    $(".sw-btn-pay").css('display', 'none');
                                }else{
                                    $(".sw-btn-next").css('display', 'block');
                                    $(".sw-btn-pay").css('display', 'block');
                                }
                                $("#price_span").html(response['total']);
                                $("#price_span_2").html(response['total']);
                                $("#price_hidden").val(response['total']);
                                $("#price_integer").val(response['price_integer']);
                                $("#formula").html(response['formula']);

                                $("#your_parking").html($("#parking_select_id option:selected").text());
                                $("#your_floor").html($("#floor_select_id").val());
                                $("#your_place").html($("#places_select").val());
                                $("#your_date_from").html($('#date_time_in').val());
                                $("#your_date_to").html($('#date_time_out').val());
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                            }
                        });
    }

    $('.arrow_icon').on('click', function () {
        $(this).siblings('select').click()
    });

    jQuery(function($){
        $('#phone_number').mask('+7 (999) 999-99-99');
    });

    jQuery.datetimepicker.setLocale('ru');

        jQuery(function(){
            jQuery('#date_time_in').datetimepicker({
                format:'Y-m-d H:i',
                minDate: 0,
                step:15,
                timepickerScrollbar: false,
                defaultTime:'09:00',
                formatTime:'H:i',

                onShow:function( ct ){
                    this.setOptions({
                        maxDate:jQuery('#date_time_out').val()?jQuery('#date_time_out').val():false,
                    })
                },
            });
            jQuery('#date_time_out').datetimepicker({
                format:'Y-m-d H:i',
                step:15,
                formatTime:'H:i',

                defaultTime:'10:00',

                timepickerScrollbar: false,
                onShow:function( ct ){
                    this.setOptions({
                        minDate:jQuery('#date_time_in').val()?jQuery('#date_time_in').val():false,
                    })
                },
            });
        });

    $(document).ready(function(){

        // Step show event
        $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
            //alert("You are on step "+stepNumber+" now");
            if(stepPosition === 'first'){
                $("#prev-btn").addClass('disabled');
            }else if(stepPosition === 'final'){
                $("#next-btn").addClass('disabled');
            }else{
                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');
            }
        });

        // Toolbar extra buttons
        var btnFinish = $('<button></button>').text('Finish')
            .addClass('btn btn-info')
            .on('click', function(){ alert('Finish Clicked'); });
        var btnCancel = $('<button></button>').text('Cancel')
            .addClass('btn btn-danger')
            .on('click', function(){ $('#smartwizard').smartWizard("reset"); });


        // Smart Wizard
        $('#smartwizard').smartWizard({
            selected: 0,
            theme: 'default',
            transitionEffect:'fade',
            showStepURLhash: true,
            toolbarSettings:{
                toolbarPosition: 'both',
                toolbarButtonPosition: 'end'
            }
        });

        // External Button Events
        $("#reset-btn").on("click", function() {
            // Reset wizard
            $('#smartwizard').smartWizard("reset");
            return true;
        });

        $("#prev-btn").on("click", function() {
            // Navigate previous
            $('#smartwizard').smartWizard("prev");
            return true;
        });

        $("#next-btn").on("click", function() {
            // Navigate next
            $('#smartwizard').smartWizard("next");
            return true;
        });

    });
  
    $('#checkout').click(function(){
        if($("#fio").val().length < 3){
            alert("Заполните поле ФИО водителя");
            return false;
        }
        if($("#nomer_auto").val().length < 3){
            alert("Заполните номер авто");
            return false;
        }
        if($("#phone_number").val().length < 3){
            alert("Заполните номер телефона");
            return false;
        }
        if($("#email").val().length < 3){
            alert("Заполните email");
            return false;
        }
        if($("#shart").is(':not(:checked)')){
            alert("Примите условия договора-оферты и политику конфиденциальности");
            return false;
        }
        send_form();
    });

    function send_form(){
        var price = parseInt($("#price_integer").val());
        var widget = new cp.CloudPayments();
            widget.charge({ // options
                publicId: 'test_api_00000000000000000000001',  //id из личного кабинета
                description: 'Оплатите '+$("#price_integer").val()+' тенге', //назначение
                amount: price, //сумма
                email: $("#email").val(),
                currency: 'KZT', //валюта
                //invoiceId: '1234567', //номер заказа  (необязательно)
                accountId: $("#email").val(), //идентификатор плательщика (необязательно)
                skin: "mini", //дизайн виджета
                data: {
                    myProp: 'myProp value' //произвольный набор параметров
                }
            },
            function (options) { // success
                $.ajax({
                            url: "/booking",
                            type: "post",
                            data: {
                                "_token": "{{csrf_token()}}",
                                "parking_id": $("#parking_select_id").val(),
                                "floor": $("#floor_select_id").val(),
                                "place": $("#places_select").val(),
                                "date_time_in": $('#date_time_in').val(),
                                "date_time_out": $('#date_time_out').val(),
                                "fio": $('#fio').val(),
                                "nomer_auto": $('#nomer_auto').val(),
                                "phone_number": $('#phone_number').val(),
                                "email": $('#email').val(),
                                "price": $("#price_hidden").val()
                            },
                            success: function (response) {
                                $("#smartwizard").css('display', 'none');
                                //$("#thanks").css('display', 'block');
                                window.location.replace("https://zhkn.kz/thank-you/");
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                            }
                        });
            },
            function (reason, options) { // fail
                //действие при неуспешной оплате
            });
    }

    if(parseInt($("#price_integer").val()) == 0 && document.location.href.indexOf('#step-2') > -1){
        window.location.assign("/form");
    }
</script>
</body>
</html>
