                    <h3 style="margin-left:20px;">Выберите свободное место в "{{$park_data[0]->name}}"</h3>
                    <div class="row" style="padding:20px;margin-top:-23px;">
                        <div class="form-group col-sm-2">
                            <label for="parking_select">Этаж:</label>
                            <select id="floor_select_id" onChange="load_places();" class="form-control" name="floor">
                                @foreach($places as $place)
                                    <option value="{{$place->floor}}">{{$place->floor}}</option>
                                @endforeach
                            </select>
                            <img src="/dist/img/down-arrow.png" class="select_arrow">

                        </div>
                        <div class="form-group col-sm-2" id="places_select_div">
                            
                        </div>
                    </div>
                    <div class="row" style="padding:20px;margin-top: -20px;">
                        <div class="col-sm-6">
                            <p><b>Стоимость места на выбранный период:</b> <br><span id="formula"></span><br><span id="price_span" class="price"></span></p>
                            <div class="alert alert-success">* Каждые неполные сутки тарифицируются как полные</div>
                        </div>
                        <div class="col-sm-6" class="info_block">
                            <p>&nbsp;<br>&nbsp;</p>
                            <button class="btn btn-secondary sw-btn-next">ДАЛЕЕ</button>
                        </div>
                    </div>