@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.parking.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.parkings.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.parking.fields.id') }}
                        </th>
                        <td>
                            {{ $parking->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.parking.fields.name') }}
                        </th>
                        <td>
                            {{ $parking->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.parking.fields.status') }}
                        </th>
                        <td>
                            {{ App\Parking::STATUS_SELECT[$parking->status] ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.parkings.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#parking_places" role="tab" data-toggle="tab">
                {{ trans('cruds.place.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="parking_places">
            @includeIf('admin.parkings.relationships.parkingPlaces', ['places' => $parking->parkingPlaces])
        </div>
    </div>
</div>

@endsection