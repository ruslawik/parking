@extends('layouts.admin')
@section('content')
@can('parking_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.parkings.create') }}">
                Добавить парковку
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        Список парковок
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Parking">
                <thead>
                    <tr>
                        <th width="10" class="noVis">

                        </th>
                        <th>
                            {{ trans('cruds.parking.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.parking.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.parking.fields.status') }}
                        </th>
                        <th class="noVis">
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($parkings as $key => $parking)
                        <tr data-entry-id="{{ $parking->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $parking->id ?? '' }}
                            </td>
                            <td>
                                {{ $parking->name ?? '' }}
                            </td>
                            <td>
                                {{ App\Parking::STATUS_SELECT[$parking->status] ?? '' }}
                            </td>
                            <td>
                              <!--
                                @can('parking_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.parkings.show', $parking->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                !-->
                                @can('parking_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.parkings.edit', $parking->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('parking_delete')
                                    <form action="{{ route('admin.parkings.destroy', $parking->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('parking_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.parkings.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 25,
  });
  let table = $('.datatable-Parking:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });

})

</script>
@endsection
