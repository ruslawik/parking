@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        Редактирование бронирования
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.orders.update", [$order->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="date_time_from">{{ trans('cruds.order.fields.date_time_from') }}</label>
                <input class="form-control datetime {{ $errors->has('date_time_from') ? 'is-invalid' : '' }}" type="text" name="date_time_from" id="date_time_from" value="{{ old('date_time_from', $order->date_time_from) }}" required>
                @if($errors->has('date_time_from'))
                    <span class="text-danger">{{ $errors->first('date_time_from') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.date_time_from_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="date_time_to">{{ trans('cruds.order.fields.date_time_to') }}</label>
                <input class="form-control datetime {{ $errors->has('date_time_to') ? 'is-invalid' : '' }}" type="text" name="date_time_to" id="date_time_to" value="{{ old('date_time_to', $order->date_time_to) }}" required>
                @if($errors->has('date_time_to'))
                    <span class="text-danger">{{ $errors->first('date_time_to') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.date_time_to_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.order.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $order->name) }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="phone">{{ trans('cruds.order.fields.phone') }}</label>
                <input class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" type="text" name="phone" id="phone" value="{{ old('phone', $order->phone) }}" required>
                @if($errors->has('phone'))
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.phone_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="car_num">{{ trans('cruds.order.fields.car_num') }}</label>
                <input class="form-control {{ $errors->has('car_num') ? 'is-invalid' : '' }}" type="text" name="car_num" id="car_num" value="{{ old('car_num', $order->car_num) }}" required>
                @if($errors->has('car_num'))
                    <span class="text-danger">{{ $errors->first('car_num') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.car_num_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="email">{{ trans('cruds.order.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email', $order->email) }}">
                @if($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="parking_id">{{ trans('cruds.place.fields.parking') }}</label>
                <select onChange="get_floors();" class="form-control select2 {{ $errors->has('parking') ? 'is-invalid' : '' }}" name="parking_id" id="parking_id" required>
                    @foreach($parkings as $id => $parking)
                        <option value="{{ $id }}" <?php if($order->place->parking_id == $id) echo 'selected'; ?>>{{ $parking }}</option>
                    @endforeach
                </select>
                @if($errors->has('parking'))
                    <span class="text-danger">{{ $errors->first('parking') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.place.fields.parking_helper') }}</span>
            </div>
            <div class="form-group" id="floors_ajax">
                <label class="required" for="floor">Этаж</label>
                <select onChange="get_places();" class="form-control select2 {{ $errors->has('floor') ? 'is-invalid' : '' }}" name="floor" id="floor" required>
                    @foreach($floors as $floor)
                        <option value="{{ $floor->floor }}" <?php if($order->place->floor == $floor->floor) echo 'selected'; ?>>{{ $floor->floor }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group" id="places_ajax">
                <label class="required" for="place_id">Место</label>
                <select class="form-control select2 {{ $errors->has('place') ? 'is-invalid' : '' }}" name="place_id" id="place_id" required>
                    @foreach($places as $id => $place)
                        <option value="{{ $id }}" {{ (old('place_id') ? old('place_id') : $order->place->id ?? '') == $id ? 'selected' : '' }}>{{ $place }}</option>
                    @endforeach
                </select>
                @if($errors->has('place'))
                    <span class="text-danger">{{ $errors->first('place') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.place_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')


    <script>
        function get_floors(){
            $.ajax({
                            url: "/admin/get-floors-to-edit",
                            type: "post",
                            data: {
                                "_token": "{{csrf_token()}}",
                                "parking_id": $("#parking_id").val(),
                            },
                            success: function (response) {
                                $("#floors_ajax").html(response);
                                get_places();
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                            }
                        });
        }
        function get_places(){
            $.ajax({
                            url: "/admin/get-places-to-edit",
                            type: "post",
                            data: {
                                "_token": "{{csrf_token()}}",
                                "parking_id": $("#parking_id").val(),
                                "floor": $("#floor").val(),
                                "date_time_from": $("#date_time_from").val(),
                                "date_time_to": $("#date_time_to").val(),
                            },
                            success: function (response) {
                                $("#places_ajax").html(response);
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                            }
                        });
        }
    </script>

@endsection