<label class="required" for="place_id">Место</label>
                <select class="form-control select2" name="place_id" id="place_id" required>
                    @foreach($places as $id => $place)
                    	@if($place->is_free($place->id, $date_from, $date_to))
                        	<option value="{{ $place->id }}">{{ $place->place }}</option>
                        @endif
                    @endforeach
                </select>
                @if($errors->has('place'))
                    <span class="text-danger">{{ $errors->first('place') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.place_helper') }}</span>