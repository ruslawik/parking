@extends('layouts.admin')
@section('content')
@can('order_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.orders.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.order.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<script type="text/javascript">
 window.setTimeout(function(){ document.location.reload(true); }, 60000);
</script>

@if(Auth::user()->roles->contains(1))
<button class="btn btn-success" data-toggle="modal" data-target="#modal-default">Сформировать отчет</button>
<br><br>
@endif

<div class="card">
    <div class="card-header">
       Список бронирований
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Order">
                <thead>
                    <tr>
                        <th width="10" class="noVis">

                        </th>
                        <th>
                            Номер брони
                        </th>
                        <th>
                            {{ trans('cruds.order.fields.date_time_from') }}
                        </th>
                        <th>
                            {{ trans('cruds.order.fields.date_time_to') }}
                        </th>
                        <th>
                            {{ trans('cruds.order.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.order.fields.phone') }}
                        </th>
                        <th>
                            {{ trans('cruds.order.fields.car_num') }}
                        </th>
                        <th>
                            {{ trans('cruds.order.fields.email') }}
                        </th>
                        <th>
                            {{ trans('cruds.order.fields.status') }}
                        </th>
                        <th>
                            {{ trans('cruds.order.fields.place') }}
                        </th>
                        <th class="noVis">
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $key => $order)
                        <tr data-entry-id="{{ $order->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $order->id ?? '' }}
                            </td>
                            <td>
                                {{ $order->date_time_from ?? '' }}
                                <hr>
                                @if($order->check_time_from == 0)
                                    <input type="checkbox" id="date_time_from" onClick="date_time_from_ajax('{{$order->id}}')">
                                @else
                                    <input type="checkbox" id="date_time_from" onClick="date_time_from_ajax('{{$order->id}}')" checked>
                                @endif
                            </td>
                            <td>
                                {{ $order->date_time_to ?? '' }}
                                <hr>
                                @if($order->check_time_to == 0)
                                    <input type="checkbox" id="date_time_to" onClick="date_time_to_ajax('{{$order->id}}')">
                                @else
                                    <input type="checkbox" id="date_time_to" onClick="date_time_to_ajax('{{$order->id}}')" checked>
                                @endif
                            </td>
                            <td>
                                {{ $order->name ?? '' }}
                            </td>
                            <td>
                                {{ $order->phone ?? '' }}
                            </td>
                            <td>
                                {{ $order->car_num ?? '' }}
                            </td>
                            <td>
                                {{ $order->email ?? '' }}
                            </td>
                            <td>
                                {{ $order->status ?? '' }}
                            </td>
                            <td>
                                {{ $order->parking_and_floor($order->place->id) ?? '' }}
                            </td>
                            <td>
                                <!--
                                @can('order_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.orders.show', $order->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                !-->
                                @can('order_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.orders.edit', $order->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('order_delete')
                                    <form action="{{ route('admin.orders.destroy', $order->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

 <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Сформировать отчет</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="/admin/export-report" method="POST">
                    {{@csrf_field()}}
                    <div class="form-group">
                        <label class="required" for="date_time_from">{{ trans('cruds.order.fields.date_time_from') }}</label>
                        <input class="form-control date" type="text" name="date_time_from" id="date_time_from" required>
                    </div>
                    <div class="form-group">
                        <label class="required" for="date_time_to">{{ trans('cruds.order.fields.date_time_to') }}</label>
                        <input class="form-control date" type="text" name="date_time_to" id="date_time_to" required>
                    </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
              <button type="submit" class="btn btn-primary">Скачать в Excel</button>
            </form>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

@endsection
@section('scripts')
@parent
<script>

    function date_time_from_ajax(order_id){
                $.ajax({
                            url: "/date_time_from_ajax",
                            type: "post",
                            data: {
                                "_token": "{{csrf_token()}}",
                                "order_id": order_id,
                            },
                            success: function (response) {

                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                            }
                });
    }

    function date_time_to_ajax(order_id){
                $.ajax({
                            url: "/date_time_to_ajax",
                            type: "post",
                            data: {
                                "_token": "{{csrf_token()}}",
                                "order_id": order_id,
                            },
                            success: function (response) {
                                
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                            }
                });
    }


    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('order_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.orders.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 25,
  });
  let table = $('.datatable-Order:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection