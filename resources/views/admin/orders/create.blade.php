@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.order.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.orders.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="date_time_from">{{ trans('cruds.order.fields.date_time_from') }}</label>
                <input class="form-control datetime {{ $errors->has('date_time_from') ? 'is-invalid' : '' }}" type="text" name="date_time_from" id="date_time_from" value="{{ old('date_time_from') }}" required>
                @if($errors->has('date_time_from'))
                    <span class="text-danger">{{ $errors->first('date_time_from') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.date_time_from_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="date_time_to">{{ trans('cruds.order.fields.date_time_to') }}</label>
                <input class="form-control datetime {{ $errors->has('date_time_to') ? 'is-invalid' : '' }}" type="text" name="date_time_to" id="date_time_to" value="{{ old('date_time_to') }}" required>
                @if($errors->has('date_time_to'))
                    <span class="text-danger">{{ $errors->first('date_time_to') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.date_time_to_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.order.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="phone">{{ trans('cruds.order.fields.phone') }}</label>
                <input class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" type="text" name="phone" id="phone" value="{{ old('phone', '') }}" required>
                @if($errors->has('phone'))
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.phone_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="car_num">{{ trans('cruds.order.fields.car_num') }}</label>
                <input class="form-control {{ $errors->has('car_num') ? 'is-invalid' : '' }}" type="text" name="car_num" id="car_num" value="{{ old('car_num', '') }}" required>
                @if($errors->has('car_num'))
                    <span class="text-danger">{{ $errors->first('car_num') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.car_num_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="email">{{ trans('cruds.order.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email') }}">
                @if($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.order.fields.status') }}</label>
                <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status" required>
                    <option value disabled {{ old('status', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Order::STATUS_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('status', 'bad') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('status'))
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.status_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="place_id">{{ trans('cruds.order.fields.place') }}</label>
                <select class="form-control select2 {{ $errors->has('place') ? 'is-invalid' : '' }}" name="place_id" id="place_id" required>
                    @foreach($places as $id => $place)
                        <option value="{{ $id }}" {{ old('place_id') == $id ? 'selected' : '' }}>{{ $place }}</option>
                    @endforeach
                </select>
                @if($errors->has('place'))
                    <span class="text-danger">{{ $errors->first('place') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.order.fields.place_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection