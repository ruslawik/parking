@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        Редактирование места
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.places.update", [$place->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="parking_id">{{ trans('cruds.place.fields.parking') }}</label>
                <select class="form-control select2 {{ $errors->has('parking') ? 'is-invalid' : '' }}" name="parking_id" id="parking_id" required>
                    @foreach($parkings as $id => $parking)
                        <option value="{{ $id }}" {{ (old('parking_id') ? old('parking_id') : $place->parking->id ?? '') == $id ? 'selected' : '' }}>{{ $parking }}</option>
                    @endforeach
                </select>
                @if($errors->has('parking'))
                    <span class="text-danger">{{ $errors->first('parking') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.place.fields.parking_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="price">{{ trans('cruds.place.fields.price') }}</label>
                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', $place->price) }}" step="1" required>
                @if($errors->has('price'))
                    <span class="text-danger">{{ $errors->first('price') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.place.fields.price_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="floor">{{ trans('cruds.place.fields.floor') }}</label>
                <input class="form-control {{ $errors->has('floor') ? 'is-invalid' : '' }}" type="number" name="floor" id="floor" value="{{ old('floor', $place->floor) }}" step="1" required>
                @if($errors->has('floor'))
                    <span class="text-danger">{{ $errors->first('floor') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.place.fields.floor_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="place">{{ trans('cruds.place.fields.place') }}</label>
                <input class="form-control {{ $errors->has('place') ? 'is-invalid' : '' }}" type="number" name="place" id="place" value="{{ old('place', $place->place) }}" step="1" required>
                @if($errors->has('place'))
                    <span class="text-danger">{{ $errors->first('place') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.place.fields.place_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection