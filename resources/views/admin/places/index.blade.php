@extends('layouts.admin')
@section('content')
@can('place_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.places.create') }}">
                Добавить место
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        Список мест
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Place">
                <thead>
                    <tr>
                        <th width="10" class="noVis">

                        </th>
                        <th>
                            {{ trans('cruds.place.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.place.fields.parking') }}
                        </th>
                        <th>
                            {{ trans('cruds.place.fields.price') }}
                        </th>
                        <th>
                            {{ trans('cruds.place.fields.floor') }}
                        </th>
                        <th>
                            {{ trans('cruds.place.fields.place') }}
                        </th>
                        <th class="noVis">
                            &nbsp;
                        </th>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                            <select class="search">
                                <option value>{{ trans('global.all') }}</option>
                                @foreach($parkings as $key => $item)
                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($places as $key => $place)
                        <tr data-entry-id="{{ $place->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $place->id ?? '' }}
                            </td>
                            <td>
                                {{ $place->parking->name ?? '' }}
                            </td>
                            <td>
                                {{ $place->price ?? '' }}
                            </td>
                            <td>
                                {{ $place->floor ?? '' }}
                            </td>
                            <td>
                                {{ $place->place ?? '' }}
                            </td>
                            <td>
                                <!--
                                @can('place_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.places.show', $place->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                !-->
                                @can('place_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.places.edit', $place->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('place_delete')
                                    <form action="{{ route('admin.places.destroy', $place->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('place_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.places.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 25,
  });
  let table = $('.datatable-Place:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  $('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value
      table
        .column($(this).parent().index())
        .search(value, strict)
        .draw()
  });
})

</script>
@endsection
