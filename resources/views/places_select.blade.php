                            <label for="parking_select">Место:</label>
                            <select id="places_select" onChange="load_price();" class="form-control" name="place">
                                @foreach($places as $place)
                                	@if($place->is_free($place->id, $date_from, $date_to))
                                    	<option value="{{$place->id}}">{{$place->place}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <img src="/dist/img/down-arrow.png" class="select_arrow">