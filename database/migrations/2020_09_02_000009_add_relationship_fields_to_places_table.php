<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPlacesTable extends Migration
{
    public function up()
    {
        Schema::table('places', function (Blueprint $table) {
            $table->unsignedInteger('parking_id');
            $table->foreign('parking_id', 'parking_fk_2108122')->references('id')->on('parkings');
        });
    }
}
