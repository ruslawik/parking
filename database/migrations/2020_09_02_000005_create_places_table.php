<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacesTable extends Migration
{
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('floor');
            $table->integer('row');
            $table->integer('place');
            $table->integer('price');
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
