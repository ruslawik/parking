<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Route::any('/form', 'Admin\FormController@index');
Route::any('/date_time_to_ajax', 'Admin\FormController@date_time_to_ajax');
Route::any('/date_time_from_ajax', 'Admin\FormController@date_time_from_ajax');
Route::post('/get-free-places-and-price', 'Admin\FormController@getPlacesAndPrice');
Route::post('/get-places-select', 'Admin\FormController@getPlacesSelect');
Route::post('/get-price', 'Admin\FormController@getPrice');
Route::post('/booking', 'Admin\FormController@booking');
Route::any('/order_bitein_dep_jatyr', 'Admin\FormController@order_bitein_dep_jatyr');

Auth::routes(['register' => false]);
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Parkings
    Route::delete('parkings/destroy', 'ParkingController@massDestroy')->name('parkings.massDestroy');
    Route::resource('parkings', 'ParkingController');

    // Places
    Route::delete('places/destroy', 'PlaceController@massDestroy')->name('places.massDestroy');
    Route::resource('places', 'PlaceController');

    // Orders
    Route::delete('orders/destroy', 'OrderController@massDestroy')->name('orders.massDestroy');
    Route::resource('orders', 'OrderController');
    Route::any('/get-floors-to-edit', 'OrderController@get_floors');
    Route::any('/get-places-to-edit', 'OrderController@get_places');
    Route::post('/export-report', 'OrderController@download_report');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});
