<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Parkings
    Route::apiResource('parkings', 'ParkingApiController');

    // Places
    Route::apiResource('places', 'PlaceApiController');

    // Orders
    Route::apiResource('orders', 'OrderApiController');
    Route::get('/getinfo/{auto_num}', 'OrderApiController@getinfo');
});
